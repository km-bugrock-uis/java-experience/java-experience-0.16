# Java Experience For 1.16

## Settings (Tweaks for the respective screens)

### Global
```
$km_global_force_gamepad_helpers: boolean   // Force the gamepad helpers to show even if there's no controller connected
```

### Start Screen
```
$km_skin_viewer_show: boolean               // Show player doll
$km_skin_viewer_hide_nametag: boolean       // Hide nametag above player doll
$km_show_playername_after_version: boolean  // Show player gametag after "Minecraft x.x.x"
$km_title_edition_java: boolean             // Show "Java Edition" below the minecraft title
$km_title_edition_cpp: boolean              // Show "C++ Edition" below the minecraft title
$km_title_edition_bedrock: boolean          // Show "Bedrock Edition" below the minecraft title
```